# GROUP TWO TO-DO APP
### ⚡ WHAT IS A TODO APP ?
A to-do app is a digital tool that helps you organize and manage your tasks, projects, and daily responsibilities. It allows you to create and prioritize a list of things you need to do, set reminders, and track your progress. You can also use a to-do app to assign tasks to others, share lists, and collaborate with team members. Overall, a to-do app is a helpful tool for individuals and teams to stay organized and productive.


### ⚡ PROJECT SUMMARY _ OUR UNDERSTANDING
Based on the given requirements, the project is to develop a to-do application using the Django framework for the back-end and JavaScript, HTML, and CSS for the front-end. The application should allow users to create and manage daily or weekly lists of tasks, mark tasks as completed or skipped, and set reminders for specific tasks via email notifications. The application should also allow users to delete tasks and automatically remove tasks older than seven days.

In terms of user stories, the project can be broken down into several stories including user account creation, task creation and management, task completion and skipping, task reminders, task deletion, and task auto-removal. Each story can be further broken down into implementation tasks such as creating database models, building user interfaces, implementing email notifications, and configuring deployment settings.

To implement this project, an agile software development framework such as Extreme Programming (XP) will be used. The development team will work collaboratively to prioritize user stories, estimate and plan implementation tasks, conduct iterative development, and continuously test and deploy the application.

The development process will be divided into sprints, each lasting approximately one to two weeks, with each sprint focusing on delivering a set of user stories. The project plan will be updated after each sprint to reflect progress made, issues encountered, and any changes to the scope or timeline.

Overall, the project aims to deliver a user-friendly and functional to-do application that meets the given requirements and adheres to best practices for software development.

### ⚡  FUNCTIONAL AND TECHNOLOGICAL  SPECIFICATION

The To Do App is going to have or use the following technological and functional constraints:

The back-end system of the to-do app should be developed using the Django framework.

The front-end system should be built using tools like JavaScript, HTML, and CSS.

The app should have a minimal user interface design.

The app should be deployed on an online server using Django/Flask framework.

The app should be developed using the Extreme Programming (XP) methodology.

The app should aim to produce higher quality software and higher quality of life for the team.

The app should be fully functional and meet the requirements outlined in the planning and design phase.

The app should be thoroughly tested to ensure it is free of bugs and errors.

The app should be easy to use and intuitive for the end-user.

Use of Restful API and methadology.

Front end and backend should be called by the above methadology (Loosely coupled).

Use of a database (PostgreSQL or MySQL)

The code for the app should be well-organized, efficient, and maintainable.


### ⚡ CI/CD & DELIVERABLE TIMELINES
The following are the deadlines for the different delivereables that will be updated with time:
- [x] Deliverable 1: Date Due: 9th March 2023 <br>
- [ ] Deliverable 2: 26th April 2023 <br>
- [ ] Deliverable 3: 31st May 2023 <br>


### ⚡ ONLINE DEMO
~~Please visit this for online demo. (url incoming)~~

### ⚡ HOW TO RUN THE APP LOCALLY
After full project: The following commands and instructions will be used to deploy and run our To-Do App locally.
- Clone the repository:<br>
``` git clone <repository_url> ```
- Install project requirements: <br>
``` pip install -r requirements.txt ```
- Migrate the database:<br>
``` python manage.py migrate ```
- Start the development server:<br>
``` python manage.py runserver ```
- Access the app:<br>
Go to the URL ```http://localhost:8000/``` to access the app. 

The port number might be different if you have specified a different port in the runserver command.

### ⚡ MORE INFORMATION
This is the current information that is due to change about this repo but more information can be got from the Group Leader: **Eddy on 0705044294**




