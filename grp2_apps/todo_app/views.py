from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return render (request,"todo_app/index.html")
def signup(request):
    return render(request, "todo_app/signup.html")

def signin(request):
    return render (request, "todo_app/signin.html")
def resetpassword(request):
    return render (request, "todo_app/resetpassword.html")

def todolist (request):
    return render (request, "todo_app/list.html")