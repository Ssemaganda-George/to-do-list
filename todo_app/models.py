from django.db import models

# Create your models here.
class Users(models.Model):
    first_name = models.CharField(max_length=64)
    surname = models.CharField(max_length=64)
    email = models.CharField(max_length=64)
    password = models.CharField(max_length=64)
     

class Todo(models.Model):
    content = models.TextField()
    