from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpRequest
from django import forms
from . import models
users = []

class NewUser(forms.Form):
    username = forms.CharField(label="New User")
    password = forms.CharField(label="password")
    confirm = forms.CharField(label="confirm")

# Create your views here.
def index(request):
    return render (request,"todo_app/index.html")

def signup(request):
    if request.method == "POST":
        form = NewUser(request.POST)
        if form.is_valid():
            user = form.cleaned_data["user"]
            users.append(user)
        else:
            return render(request, "todo_app/signup.html", {
                "form": form
            })
    return render(request, "todo_app/signup.html", {
        "form": NewUser()
    })

def signin(request):
    return render (request, "todo_app/signin.html")
def resetpassword(request):
    return render (request, "todo_app/resetpassword.html")

def view_items(request):
    items = {'todo_list' : Todo.objects.all()}

    return render (request, "todo_app/todolist.html", items)

def insert_item(request:HttpRequest):
    content = request.POST['content']
    todo = Todo(content = request.POST['content'])
    todo.save()
    return redirect("todo_app/view_items")

def delete_item(request, todo_id):
    todo_to_delete = Todo.objects.get(id=todo_id)
    todo_to_delete.delete()
    return redirect("todo_app/view_items")